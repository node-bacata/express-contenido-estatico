const express = require('express')
const app = express()
const port = 8080


app.use(express.static('public'))

app.get('/', (req, res) => {
  res.send('Hi, Andres')
})


app.get('*', (req, res) => {
    res.sendFile( __dirname + '/public/404.html'); 
  })

app.listen(port, () => {
  console.log(`Este ejemplo se està ejecutando en:  http://localhost:${port}`)
})





